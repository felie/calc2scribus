#! /bin/bash

# convert document.ods into html
libreoffice --headless --convert-to html document.ods
./calc2zones.py document.html 3 9 > document.zones

retVal=$?
if [ $retVal -ne 0 ]; 
then
	echo "Error in calc2zones"
 	echo "Quit the script before zone2scribus execution"
	exit
fi
cat document.zones

# avoid QT debugging informations
export QT_LOGGING_RULES="qt5ct.debug=false"
cd $data
scribus --no-gui --no-splash -py zones2scribus.py document.sla document.zones 2> zone2scribus.log
# To hide the gui properly, prefix the above with xvfb-run, but only if there is no font error at the beginning

retVal=$?
if [ $retVal -ne 0 ]; 
then
        echo "Error in zones2scribus"
        echo "See in zones2scribus.log file where is the problem"
        exit
fi

#scribus copiedoc.sla
