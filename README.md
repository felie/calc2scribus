# Que fait calc2scribus ?

## Exploitez la rapidité de travail sur tableur

Imaginez une feuille de calcul ou vous représentez les pavés sur les pages par des fusions de cellule comme sur cette image:

Contraintes: pour les noms de contenu dans les cellules du tableur, pas d'accents ou de caractères spéciaux (mais pas de problème pour les espaces)

![](images/pages.png)

## Sauvegardez votre feuille de calcul en HTML.

## Lancez ./run.sh

Et voilà le résultat en quelques secondes!

![](images/result.png)

# Comment ça marche

## Il y a des fichiers d'exemples dans le répertoire demo

## Deux scripts

Le programme **calc2scribus** se compose de deux scripts, qui sont lancés successivement par run.sh
- calc2zones qui produit un fichier de zones **document.zones**
- zones2scribus qui crée les cadres pour chaque zone dans un document scribus **document.sla** et les remplit si possible avec leur contenu. Pour fabriquer votre document.sla, vous pouvez faire une copie de model.sla.

Comme on le voit sur la feuille de calcul, chaque pavé contient un nom **unique**.

Prenons l'exemple du cadre *bonhomme*

Au moment de remplir les cadres, le système va chercher des noms de textes (bonhomme.odt, bohomme.html, bonhomme.md ou bonhomme.txt) et des noms d'images (bonhomme.eps, bonhomme.jpg ou bonhomme.png).

Remarque: les formats sont utilisés dans cet ordre de priorité (ex: si deux images sont trouvées, il prend le premier des formats)
- odt > html > md > txt
- eps > jpg > png

- S'il trouve une image, il la mettra en place
    - S'il trouve aussi un texte, il le mettra *autour* de l'image
- S'il ne trouve pas d'image
    - S'il trouve un texte, il le placera
        - Sinon il mettra du faux texte
	
Techniquement Le système fabriquera dans scribus pour chaque pavé portant un nom
- systématiquement un cadre de texte sous ce nom
- *éventuellement* un cadre d'image sous le nom *IMG nom*

### calc2zones <fichier.html> > zones

va produire une liste des zones qu'il a repéré dans l'export HTML de la feuille de calcul, voici le fichier de zone pour notre exemple:

</pre>
1 ; 0 ; 0 ; 3 ; 9 ; idees recues
2 ; 0 ; 0 ; 2 ; 6 ; histoire de fou
2 ; 2 ; 0 ; 1 ; 4 ; bonhomme
3 ; 0 ; 0 ; 3 ; 4 ; article de fond
4 ; 0 ; 0 ; 2 ; 9 ; forge
4 ; 2 ; 0 ; 1 ; 9 ; faits divers
2 ; 2 ; 4 ; 1 ; 2 ; test
3 ; 0 ; 4 ; 3 ; 5 ; remplissage
2 ; 0 ; 6 ; 3 ; 3 ; bidule
</pre>

Chaque ligne se lit ainsi: page, x,y,largeur,hauteur,contenu

### zones2scribus <document scribus> <fichier de zone>

**zone2scribus** est exécuté ensuite comme un script scribus, qui va disposer les zones où il faut, et charger les contenus s'il existent.

Dépendances
- pandoc - pour la conversion systématique des fichiers html et md en odt
- ImageMagick - (pour *convert*) afin de convertir toutes les images en eps

<pre>
sudo apt install pandoc imagemagick
</pre>

# Récapitulatif

## Un seul script qui fait tout!

Voici ce que fait ./run.sh

* production des zones via le script calc2zones
* lancement de zones2scribus par scribus en mode automatique
    * production des cadres dans les bonnes pages
    * injection dans les cadres des texte et des images
        * du texte (au format odt, html, md ou txt si fichier de ce nom trouvé ou du *lorem ipsum* sinon)
	* ou de l'images (eps, jpg ou png si trouvé ou rien sinon)

Pensez à rendre exécutables vos fichiers run, calc2zones et zones2scribus:
<pre>
chmod +x run calc2zones zones2scribus
</pre>

# Et la mise à jour ?

Et les textes ou images qui manquaient ? Et qui du déplacement et retaillage des pavés

## Il suffit de relancer **./run.sh**

Cela mettra à jour les contenus, et déplacera éventuellement les cadres.


Pour les images **déjà trouvées** et présentes dans le document, il suffira que scribus les trouve modifiées pour qu'il les mette à jour automatiquement. Mais il n'en va pas de même pour les textes ou pour les images qu'il n'avait pas trouvé. Comment mettre à jour plus tard le *lorem ipsum* par le contenu souhaité ? Comment mettre la nouvelle image d'illustration d'un pavé ?

- Si l'on veut être sûr que zone2scribus ne touchera pas au contenu d'un cadre *nom*, il suffit qu'il n'existe pas de fichier *nom.odt*, *nom.html*, *nom.md* ou *nom.txt*. Le mieux est de créer un répertoire archives où on les déplacera.
- Si l'on veut verrouiller un cadre pour qu'il ne puisse plus être déplacé, il suffit de le préfixer d'une étoile dans la feuille de calcul.

Prenons un cas concret: on a travaillé sur des bouts de textes du document de démonstration, dont un qui porte le même nom qu'une image (l'image s'appelait bonhomme.eps, le texte bonhomme.txt)

Voilà ce que l'on obtient après l'exécution de *update.sh*: il a mis à jour trois cadres.

![](images/result2.png)

# Pour installer les polices manquent (si scribus bloque à la production du document)

- Télécharger le zip du fichier de la police manquante
- Dézippez le
- copier le répertoire contenant les *ttf là où votre système le cherchera, c'est à dire dans /usr/share/fonts/truetype

# Cas testés

Il est possible pour un cadre **qui porte un nom** (sinon la place restera complètement vide)
- de ne rien trouver du tout au passage de zones2scribus: il mettra alors du faux texte
- d'avoir un texte, puis d'ajouter plus tard une image
- avoir une image et d'ajouter plus tard un texte
- d'avoir une image seulement
- d'avoir un texte seulement
- d'avoir les deux d'emblée

# En résumé

Disposer des zones par fusion de cellule dans un tableur est très rapide (et plus rapide encore si on se fait des raccourcis clavier pour *fusionner les cellules* et *scinder les cellules*. Cela permet de réaliser très vite un chemin de fer pour disposer les pavés de texte par leur nom.

Le document scribus est ensuite produit et mis à jour automatiquement avec un seul script.

# TODO

- Les zones chaînées. Il suffira de les désigner dans la feuille de calcul par le même nom et des indices. Riri sera le nom du premier cadre (et du document), riri1 riri2 riri3 seront les suivants.






