#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright 2021 François Elie francois@elie.org

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le
modifier au titre des clauses de la Licence Publique Générale GNU, telle
que publiée par la Free Software Foundation ; soit la version 2 de la
Licence, ou (à votre discrétion) une version ultérieure quelconque.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS
AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIABILITE ou
DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la Licence Publique
Générale GNU pour plus de détails.

Vous devriez avoir reçu un exemplaire de la Licence Publique Générale GNU
avec ce programme ; si ce n'est pas le cas, écrivez à la Free Software
Foundation Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
"""

from collections import defaultdict
import re
import sys

# pour écrire en sortie en utf-8
sys.stdout.reconfigure(encoding='utf-8')

# given an html file (from a calc spreadsheet, produce a table of zone for scribus
# see zones2scribus

# thanks to reclosedev 2012 for parsing-a-table-with-rowspan-and-colspan

def table_to_list(table):
    dct = table_to_2d_dict(table)
    return list(iter_2d_dict(dct))


def table_to_2d_dict(table):
    result = defaultdict(lambda : defaultdict(str))
    for row_i, row in enumerate(table.xpath('./tr')):
        for col_i, col in enumerate(row.xpath('./td|./th')):
            colspan = int(col.get('colspan', 1))
            rowspan = int(col.get('rowspan', 1))
            col_data = col.text_content()
            while row_i in result and col_i in result[row_i]:
                col_i += 1
            for i in range(row_i, row_i + rowspan):
                for j in range(col_i, col_i + colspan):
                    result[i][j] = col_data
    return result


def iter_2d_dict(dct):
    for i, row in sorted(dct.items()):
        cols = []
        for j, col in sorted(row.items()):
            cols.append(col)
        yield cols


if __name__ == '__main__':
    import lxml.html
    if len(sys.argv)<2:
        print('Usage: ./calc2scribus.py <file> [<width> <height>]')
        print('---------------------------------------')
        print('You can give also the pages disposition on the spreadsheet')
        print('        width of a grid page: gx (default=3)')
        print('       height of a grid page: gy (default=9)')
        exit()
    gx=3
    gy=9
    if len(sys.argv) >2:
        gx=int(sys.argv[2])
    if len(sys.argv) >3:
        gy=int(sys.argv[3])
    doc = lxml.html.parse(sys.argv[1])
    # normalize the html table in table
    table=[]
    for table_el in doc.xpath('//table'):
        table = table_to_list(table_el)
    #print(table)
        
    # produce the list zone in the form: x,y,with,height,name (the name is the cell content) 
    zone=[]
    bx=[]
    by=[]
    lx=[]
    ly=[]
    page=[]
    for y in range(len(table)):
        for x in range(len(table[y])):
            el=table[y][x]
            if not(el.strip()==''):
                if not (el in zone):
                    zone.append(el)
                    bx.append(x)
                    by.append(y)
                    lx.append(x)
                    ly.append(y)
                    page.append(' ')
                else:
                    i=zone.index(el)
                    if lx[i]<x:
                        lx[i]=x
                    if ly[i]<y:
                        ly[i]=y
    htx = gx + 1 # largeur hors tout
    hty = gy + 1
    for i in range(len(zone)):
        lx[i]=lx[i]-bx[i]+1
        ly[i]=ly[i]-by[i]+1
        by[i]=by[i]-1;
        page[i]=(bx[i] // htx)+1
        # exclude lines and colums for searation between pages
        if (bx[i] % htx) < gx and (by[i] % hty) < gy and by[i]<hty: # if its no a comment in margin or below the line of pages
            print(page[i],';',bx[i] % htx,';',by[i] % hty,';',lx[i],';',ly[i],';',zone[i])
