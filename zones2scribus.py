#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright 2021 François Elie francois@elie.org

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le
modifier au titre des clauses de la Licence Publique Générale GNU, telle
que publiée par la Free Software Foundation ; soit la version 2 de la
Licence, ou (à votre discrétion) une version ultérieure quelconque.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS
AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIABILITE ou
DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la Licence Publique
Générale GNU pour plus de détails.

Vous devriez avoir reçu un exemplaire de la Licence Publique Générale GNU
avec ce programme ; si ce n'est pas le cas, écrivez à la Free Software
Foundation Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
"""
gx = 3 # width of the grid page
gy = 9 # height of the grid page
padding = 5
margin = 10
# width = 210 # UNIT_MILLIMETERS
# height = 297 # UNIT_MILLIMETERS
width = 595.276  # UNIT_POINTS
height = 841.89  # UNIT_POINTS

# Requires Scribus >= 1.5.1
# Tested on Scribus 1.5.5

# Usage:
#     scribus --no-gui --no-splash -py zone2scribus.py document.sla document.zones
# To hide the gui properly, prefix the above with xvfb-run, but only if there is no font error at the beginning

import scribus
import re
import sys
import os.path
import subprocess
from PIL import Image

# pour écrire en sortie en utf-8
#sys.stdout.reconfigure(encoding='utf-8')

#newDoc(PAPER_A4, (0, 0, 0, 0), PORTRAIT, 1, UNIT_POINTS, FACINGPAGES, FIRSTPAGERIGHT) 
#newDocument(PAPER_A4, (10, 10, 20, 20), LANDSCAPE, 7, UNIT_POINTS, PAGE_4, 3, 1)
#newPage(-1)

# to be executed in the data path!
if len(sys.argv)<3:
    print('2 parameters required:  <.sla> and <zones list>')
    exit()
    
print '--------- zones2scribus'

if not os.path.isfile(sys.argv[1]):
    newDocument(PAPER_A4, (0,0,0,0), PORTRAIT, 7, UNIT_POINTS, PAGE_2, 1, 64)
    print 'creation of document.sla'
    saveDocAs(sys.argv[1])
else:
    print 'open document.sla'
    scribus.openDoc(sys.argv[1])

textframe = []
imageframe = []

# recensement des cadres exixtants
page = 1
pagenum = scribus.pageCount()
content = []
while (page <= pagenum):
    scribus.gotoPage(page)
    d = scribus.getPageItems()
    for item in d:
        if item[1] == 4: # it's a text 
            textframe.append(item[0])
        else:
            imageframe.append(item[0])
    page += 1

print textframe
print imageframe

# produce a .sla with zone defined in the file zones
with open(sys.argv[2]) as f:
    zones = f.read()

z=zones.split('\n')
print('nombre brut de zones:',len(z))
# à cause de la dernière ligne vide, suppression du dernier élément
z.pop()

# type and conversion of coordinates
for i in range(len(z)):
    z[i]=z[i].split(';')
    # remove spaces around zone's name
    z[i][5]=z[i][5].strip()
    dx=(width-2*margin)/gx 
    dy=(height-2*margin)/gy
    e=z[i]
    e[1] = margin+int(e[1])*dx+padding
    e[2] = margin+int(e[2])*dy+padding
    e[3] = int(e[3])*dx-2*padding
    e[4] = int(e[4])*dy-2*padding
    for j in range(5):
        #print('*',z[i][j],'*')
        if j==0:
            z[i][j] = int(z[i][j])
        else:
            z[i][j] = float(z[i][j])
            
scribus.createParagraphStyle("joli", 1,0,3,0,0,0,0,20)
scribus.createCharStyle("titre", "DejaVu Sans Condensed Bold", 18)

# placeEPS("test.eps", 10, 10)

def load_an_image(name,suffix,e):
    if not (suffix == 'png'):
        print 'convert image',name+'.suffix to png'
        print 'convert "'+name+'.'+suffix+'" "'+name+'.png"'
        subprocess.call('convert "'+name+'.'+suffix+'" "'+name+'.png"',shell=True)
    img = Image.open(name+'.png')
    w,h = img.size
    frame='IMG '+name
    if not (frame in imageframe):
        print 'create a new image frame',name
        # createImage(e[1],e[2],w/3,h/3,'IMG '+name)  # conversion point en mm
        createImage(e[1],e[2],e[3]/2,e[4]/2,'IMG '+name)  # image à la moitié de la frame
    else:
        moveObjectAbs(e[1],e[2],frame)
        # sizeObject(w,h,frame)
    loadImage(name+'.png',frame)
    textFlowMode(frame,3)
    setScaleImageToFrame(True, True,frame)
    
contents=open('contents', 'w')

def read_text(name,suffix,reader):
    if not (suffix == 'odt'):
        subprocess.call('pandoc -f '+reader+' -t odt "'+name+'.'+suffix+'" -o "'+name+'.odt"',shell=True)
        print 'convert text ',name+'.suffix to .odt'
    print 'inject text ',name+'.odt into frame',name
    insertHtmlText(name+'.odt',name)

for i in range(len(z)):
    e=z[i]
    name=e[5]
    print e
    contents.write(name+'\n') # all the name of the texts/frames are recorded in contents for update
    scribus.gotoPage(e[0])
    okImage = os.path.isfile('./'+name+'.eps') or os.path.isfile('./'+name+'.jpg') or os.path.isfile('./'+name+'.png')
    # traitement des textes, priorités: html > txt
    if not (name in textframe):
        createText(e[1],e[2],e[3],e[4],name)  # on crée de toute façon un cadre de texte, au cas ou un image a un texte
        print 'create a new texte frame',name
    else:
        moveObjectAbs(e[1],e[2],name)
        sizeObject(e[3],e[4],name)
    if os.path.isfile(name+'.odt'):
        read_text(name,'odt','')
    elif os.path.isfile(name+'.html'):
        read_text(name,'html','html')
    elif os.path.isfile(name+'.md'):
        read_text(name,'md','markdown')
    elif os.path.isfile(name+'.txt'):
        setText(open(name+'.txt').read(),name)
    else:
        if not okImage:
            insertHtmlText('lorem.html', name)
            print 'inject lorem ipsum and add title into frame',name
            # met le titre du cadre dans le cas d'un lorem ipsum pour repérage du cadre
            insertText(name+'\n', 0, name)
            scribus.selectText(0,len(name), name)
            scribus.setCharacterStyle('titre', name)
            scribus.selectText(len(name)+1,getTextLength(name)-1-len(name)-1, name)
    setStyle('joli',name)
    setLineColor("Black",name)
    # traite les images, priorité: eps > jpg > png (on les traite après, pour qu'elles soient toujours au dessus des textes)
    if os.path.isfile(name+'.png'):
        okImage=True
        load_an_image(name,'png',e) 
    elif os.path.isfile(name+'.jpeg'):
        okImage=True
        load_an_image(name,'jpeg',e)
    elif os.path.isfile(name+'.jpg'):
        okImage=True
        load_an_image(name,'jpg',e)       
  
contents.close()

#pdf = scribus.PDFfile()
#pdf.file = 'output1.pdf'
#pdf.save()
scribus.saveDoc()
print 'save .sla document'
